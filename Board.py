from random import uniform, randint, shuffle
from copy import deepcopy

class Board:

    def __init__(self, values):
        self.values = deepcopy(values)
        self.given_board = values
        self.fitness_score = None

    def __str__(self):
        """
        This function will be useful for printing the answer
        """
        final_message = str()
        for _ in range(9):
            final_message += ' ---'
        final_message += '\n'
        for row in self.values:
            final_message += '|'
            for number in row:
                final_message += f' {number} |'
            final_message += '\n'
            for _ in range(9):
                final_message += ' ---'
            final_message += '\n'
        return final_message

    def check_duplication_for_generation(self, row, column, value):
        """
        This function checks whether adding this to the cell is valid or not
        """
        if self.given_board[row][column] != 0:
            return False
        if self.check_column_duplication(column, value) is True:
            return False
        if self.check_row_duplication(row, value) is True:
            return False
        if self.check_subgrid_duplicatoin(row, column, value) is True:
            return False
        return True

    def fill_board(self):
        """
        This function fills the empty cells with random values, such that 
        there is no duplication in each row
        """
        # This list has possible answers for each cell of the sudoku
        helper_board = Board([[list() for _ in range(9)] for _ in range(9)])

        # This piece of code, generate possible numbers for each cell
        for row in range(9):
            for column in range(9):
                for value in range(1, 10):
                    if self.check_duplication_for_generation(row, column, value):
                        helper_board.values[row][column].append(value)
                    elif self.given_board[row][column] != 0:
                        helper_board.values[row][column].append(self.given_board[row][column])
                        break
        
        # This piece of code fills the sudoku table
        for row in range(9):
            this_row = [0 for _ in range(9)]
            for column in range(9):
                # This means this cell is filled by the question
                if self.given_board[row][column] != 0:
                    this_row[column] = self.given_board[row][column]
                # This means this cell has to be filled randomly
                elif self.given_board[row][column] == 0:
                    this_row[column] = \
                    helper_board.values[row][column][randint(0, len(helper_board.values[row][column]) - 1)]

            # This means this row has duplicat numebrs      
            while len(set(this_row)) != 9:
                for column in range(9):
                    # We change the numbers those are not given by the question, since we have an acceptable answer
                    if self.given_board[row][column] == 0:
                        this_row[column] = \
                        helper_board.values[row][column][randint(0, len(helper_board.values[row][column]) - 1)]
            self.values[row] = this_row

    def check_row_duplication(self, row, value):
        """
        When we want to add an item to the board, this function checks
        whether this item is currently in the specified row or not
        """
        return True if value in self.given_board[row] else False

    def check_column_duplication(self, column, value):
        """
        When we want to add an item to the board, this function checks
        whether this item is currently in the specified column or not
        """
        return True if value in [self.given_board[row][column] for row in range(9)] else False

    def check_subgrid_duplicatoin(self, row, column, value):
        """
        When we want to add an item to the board, this function checks
        whether this item is currently in the specified subgrid or not
        """
        subgrid_row, subgrid_column = row // 3 * 3, column // 3 * 3
        subgrid_values = [self.given_board[subgrid_row + row][subgrid_column + column] for row in range(3) for column in range(3)]
        return True if value in subgrid_values else False

    def __calculate_column_fitness_score(self):
        """
        In order to find total fitness score, fitness score of each column
        has to be calculated and summed up with each other 
        """
        column_sum = 0
        for column in range(9):
            column_count = [0 for _ in range(9)]
            for row in range(9):
                column_count[self.values[row][column] - 1] += 1
            column_sum += (1.0 / len(set(column_count))) / 9
        return column_sum

    def __calculate_subgrid_fitness_score(self):
        """
        In order to find total fitness score, fitness score of each subgrid
        has to be calculated and summed up with each other 
        """
        subgrid_sum = 0
        for subgrid in range(9):
            subgrid_count = [0 for _ in range(9)]
            for row in range(3):
                for column in range(3):
                    subgrid_count[self.values[subgrid // 3 * 3 + row][subgrid % 3 * 3 + column] - 1] += 1
            subgrid_sum += (1.0 / len(set(subgrid_count))) / 9
        return subgrid_sum
            
    def update_fitness_score(self):
        """
        This function calculates the fitness score of this board in order
        to find out how close it is to being the final and correct answer
        """
        column_fitness_score = self.__calculate_column_fitness_score()
        subgrid_fitness_score = self.__calculate_subgrid_fitness_score()
        if int(column_fitness_score) == 1 and int(subgrid_fitness_score) == 1:
            self.fitness_score = 1
        else:
            self.fitness_score = column_fitness_score * subgrid_fitness_score

    def __check_duplication_for_mutation(self, row, from_column, to_column):
        """
        This function check whether mutation cause duplication in columns
        and subgrids or not
        """
        if self.check_column_duplication(from_column, self.values[row][to_column]) is True:
            return False
        if self.check_column_duplication(to_column, self.values[row][to_column]) is True:
            return False
        if self.check_subgrid_duplicatoin(row, from_column, self.values[row][from_column]) is True:
            return False
        if self.check_subgrid_duplicatoin(row, to_column, self.values[row][to_column]) is True:
            return False
        return True

    def mutate(self, mutation_rate):
        """
        This function performs a mutation on the board, in order to do that
        this function swaps two numbers in a random row
        """
        probablity = uniform(0, 1)
        was_it_successful = False
        if probablity < mutation_rate:
            # Generate random numbers till a mutation happens
            while not was_it_successful:
                selected_row = randint(0, 8)
                from_column = randint(0, 8)
                to_column = randint(0, 8)
                while from_column == to_column:
                    to_column = randint(0, 8)
                # This if checks whether this cell has been given or not
                if self.given_board[selected_row][from_column] == 0 and self.given_board[selected_row][to_column] == 0:
                    # This if checks after mutation, whether duplication will be caused
                    if self.__check_duplication_for_mutation(selected_row, from_column, to_column):
                        self.values[selected_row][to_column], self.values[selected_row][from_column] = \
                        self.values[selected_row][from_column], self.values[selected_row][to_column]
                        was_it_successful = True
