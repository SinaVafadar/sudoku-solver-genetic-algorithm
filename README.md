# sudoku-solver-genetic-algorithm
This project is capable of solving a Sudoku puzzle using a genetic algorithm. The puzzle configuration is a 2d grid.
After an instance of Sudoku is created and an input grid is passed to it, solve_sudokue() can be called to get a solved grid.
