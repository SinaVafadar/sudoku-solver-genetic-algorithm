from Board import Board

from random import uniform, randint
from copy import deepcopy


class CrossOver:

    def tournament_selection(self, candidates, selection_rate):
        """
        This function selects one candidate among all candidates
        based on tournament algorithm
        """
        # Selects two random candidates
        first_candidate = candidates[randint(0, len(candidates) - 1)]
        second_candidate = candidates[randint(0, len(candidates) - 1)]
        first_fitness, second_fitness = first_candidate.fitness_score, second_candidate.fitness_score
        if first_fitness > second_fitness:
            fittest = first_candidate
            weakest = second_candidate
        else:
            fittest = second_candidate
            weakest = first_candidate
        # Returns one of them based on their fitness score and selection rate
        probability = uniform(0, 1)        
        if probability < selection_rate:
            return fittest
        else:
            return weakest

    def cross_over(self, parent_one, parent_two, cross_over_rate):
        """
        This function will produce two children, based on two parents
        """
        child_one, child_two = Board(parent_one.given_board), Board(parent_two.given_board)
        child_one.values = deepcopy(parent_one.values)
        child_two.values = deepcopy(parent_two.values)
        probability = uniform(0, 1)
        if probability < cross_over_rate:
            # Two cut off will be chosen to merge the parent between these two
            cross_over_point_one = randint(0, 8)
            cross_over_point_two = randint(1, 9)
            while cross_over_point_one == cross_over_point_two:
                cross_over_point_two = randint(1, 9)
            if cross_over_point_one > cross_over_point_two:
                cross_over_point_one, cross_over_point_two = \
                cross_over_point_two, cross_over_point_one
            # The merge will be done based on __cross_over_rows() function for each row
            for row_number in range(cross_over_point_one, cross_over_point_two):
                child_one.values[row_number], child_two.values[row_number] = \
                self.__cross_over_rows(child_one.values[row_number], child_two.values[row_number])
        return child_one, child_two
            
    def __cross_over_rows(self, row_one, row_two):
        """
        This function merges to row of the two parents and
        generates two childs
        """
        child_one_row = [0 for _ in range(9)]
        child_two_row = [0 for _ in range(9)]
        remaining_numbers = [number for number in range(1, 10)]
        # This value will be used to mix two rows almost random
        cycle_number = 0
        # This while will be contined till both of childs are filled with numbers 1 to 9
        while 0 in child_one_row and 0 in child_two_row:
            # Find the first number in the first parent row which is in remaining_numbers, too
            # This means, this line find the first number which is not placed in the child list
            # Note that we use the index of this number
            index = self.__find_unused_number(row_one, remaining_numbers)
            first_number = row_one[index]
            # We want to use selected number in the child, hence, we cannot use it again.
            # So, we remove the number from remainin_numbers
            remaining_numbers.remove(first_number)
            # Based on the cycle_number value, we decide how to produce the child
            # If cycle number is even, we do the code below
            if cycle_number % 2 == 0:
                child_one_row[index], child_two_row[index] = row_one[index], row_two[index]
            # If the cycle_number is odd, we do the code below
            # Deciding based on cycle_number will help us to generate more rendom boards
            # And this can help us to solve the sudoku more quickly
            else:
                child_one_row[index], child_two_row[index] = row_two[index], row_one[index]
            # Note that we find the index of the first number which was available in the first row
            # This index in the second row does not necesserily have the same value
            # If they have the same value this means they are maybe given by the question
            # If they are not the same, we are sure that they are not the same
            # So we calculate this value in the second list, too
            next_numebr = row_two[index]
            # And while they are not the same, this means they are not given by the question
            # So, we can shuffle those. The code below, do this task
            # If they become the same as each other, the while loop will be broken
            while next_numebr != first_number:
                # Because previous numbers were not the same as each other, we have to remove
                # both numbers from remaining, and use both numbers in both children
                # Since we use first_number and next_number in just one child, we have to
                # find those numbers again, and use them once again, and after that, remove
                # the number from remaining_numbers
                index = self.__find_index(row_one, next_numebr)
                if cycle_number % 2 == 0:
                    child_one_row[index], child_two_row[index] = row_one[index], row_two[index]
                    remaining_numbers.remove(row_one[index])
                else:
                    child_one_row[index], child_two_row[index] = row_two[index], row_one[index]
                    remaining_numbers.remove(row_one[index])
                next_numebr = row_two[index]
            cycle_number += 1
        return child_one_row, child_two_row

    def __find_unused_number(self, parent_row, remaining_numbers):
        """
        This functoin checks the parent_row numebrs to find out
        that there is any of them in the remainig_number array
        and returns the index of that number
        """
        for index, number in enumerate(parent_row):
            if number in remaining_numbers:
                return index

    def __find_index(self, parent_row, value):
        """
        This functoin checks whether a specfic value is in 
        parent_row or not and returns the index of that numebr
        """
        for index, number in enumerate(parent_row):
            if number == value:
                return index
