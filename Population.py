from Board import Board


class Population:

    def __init__(self, size_of_population, given_board):
        """
        This function generates number of randomly filled boards
        and calculates their fitness scores
        """
        self.size_of_population = size_of_population
        self.candidates = list()

        print(f'Start generatinig population with size {self.size_of_population}')
        print('This may take a while, please be patient...')
        percentage = 1
        for counter in range(self.size_of_population):
            if counter == percentage * self.size_of_population // 10:
                print(f'{percentage * 10}% of all candidates are generated...')
                percentage += 1
            this_candidate = Board(given_board)
            this_candidate.fill_board()
            self.candidates.append(this_candidate)
        self.update_fitnesses()
        print(f'{self.size_of_population} boards were generated successfully')

    def update_fitnesses(self):
        """
        This function calls update_fitness_score() of each board
        to update all fitness scores
        """
        for candidate in self.candidates:
            candidate.update_fitness_score()

    def sort_based_on_fitness_score(self):
        """
        This function sorts all the boards based on their fitnsess score
        """
        self.candidates = sorted(self.candidates, key=lambda item: item.fitness_score, reverse=True)